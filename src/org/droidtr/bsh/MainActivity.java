package org.droidtr.bsh;

import android.app.Activity;
import android.os.Bundle;
import bsh.Interpreter;
import android.widget.Toast;
import java.io.*;

public class MainActivity extends Activity {
Interpreter i = new Interpreter();
	public void onCreate(Bundle instance){
		this.requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"},1);
		 File file = new File("/sdcard/script",
        "main.bsh");
         try {
                Reader reader = new FileReader(file);
                i.set("MainActivity", this);
                i.set("bundle",instance);
                i.eval(reader);
                i.source(file.getAbsolutePath());
            } catch (Exception e) {
                Toast.makeText(this, e.toString(),  Toast.LENGTH_LONG).show();
                finish();
            }
		super.onCreate(instance);
	}

}
