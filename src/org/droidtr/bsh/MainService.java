package org.droidtr.bsh;

import android.app.Service;
import android.util.Log;
import android.os.IBinder;
import bsh.Interpreter;
import java.io.*;
import android.content.Intent;

public class MainService extends Service {
  Interpreter i = new Interpreter();
  IBinder mBinder;  
  @Override
  public IBinder onBind(Intent arg0) {
        return mBinder;
  }
  @Override
  public void onCreate() {
		 File file = new File("/sdcard/script",
        "service.bsh");
         try {
                Reader reader = new FileReader(file);
                i.set("MainService", this);
                i.eval(reader);
                i.source(file.getAbsolutePath());
            } catch (Exception e) {
				 Log.i("TAG",e.toString());
            }
	}

}
