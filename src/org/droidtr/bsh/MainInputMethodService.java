package org.droidtr.bsh;

import android.app.*;
import android.content.res.*;
import android.inputmethodservice.*;
import android.os.*;
import android.text.*;
import android.text.method.*;
import android.view.*;
import android.view.inputmethod.*;
import java.util.*;
import android.widget.*;
import android.util.Log;
import java.io.*;
import bsh.Interpreter;

public class MainInputMethodService extends InputMethodService{
  Interpreter i = new Interpreter();
    @Override
	public View onCreateInputView(){
		 View v = new View(this);
		 File file = new File("/sdcard/script",
        "keyboard.bsh");
         try {
                Reader reader = new FileReader(file);
                i.set("MainService", this);
                i.set("InputView", v);
                i.eval(reader);
                i.source(file.getAbsolutePath());
         } catch (Exception e) {
			 Log.i("TAG",e.toString());
         }
         return v;
	}
	@Override 
	public void onCreate() {
		super.onCreate();
	}
}
